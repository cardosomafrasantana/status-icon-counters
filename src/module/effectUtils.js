import { DurationType } from "./durationType.js";

/**
 * Returns the first effect with the given image path for the given actor.
 * @param {Actor} actor The actor to read the effects from.
 * @param {string} img The image path of the status to retrieve.
 * @returns {ActiveEffect?}
 */
export function findEffectByImg(actor, img) {
    if (!actor) return null;
    let effect = actor.effects.find(effect => effect.img === img);
    if (game.system.id === "sfrpg") effect ??= actor.items.find(item => item.type === "effect" && item.img === img);
    effect ??= actor.temporaryEffects.find(effect => effect.img === img);
    return effect;
}

/**
 * Returns the first effect with the given status id for the given actor.
 * @param {Actor} actor The actor to read the effects from.
 * @param {string} statusId The id of the status to retrieve.
 * @returns {ActiveEffect?}
 */
export function findEffectById(actor, statusId) {
    if (!actor) return null;
    let effect = actor.effects.find(effect => effect.statuses.has(statusId));
    if (game.system.id === "sfrpg") effect ??= actor.items.find(item => item.type === "effect" && item.name.toLowerCase() === statusId);
    effect ??= actor.temporaryEffects.find(effect => effect.statuses.has(statusId));
    return effect;
}

/**
 * Creates a duration for use in an ActiveEffect.
 * @param {number} value The duration to initialize.
 * @param {DurationType} type The type of the duration.
 * @returns {object} An effect duration object representing the value.
 */
export function createDuration(value, type) {
    const duration = {};
    if (type === DurationType.Round) {
        duration.rounds = value;
        duration.turns = 0;
    } else {
        duration.rounds = 0;
        duration.turns = value;
    }

    // Set start round and turn to current combat state.
    const combat = game.combat;
    if (combat && combat.active && combat.round !== 0) {
        duration.combat = combat._id;

        duration.startRound = combat.round ?? 1;
        duration.startTurn = combat.turn ?? 0;
    } else {
        duration.startRound = 1;
        duration.startTurn = 0;
    }

    return duration;
}
